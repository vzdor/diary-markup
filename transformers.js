'use strict';

const {split} = require('./regexp-utils'),
      stemmer = require('stemtiny/stemmer')

const urlRe = /[a-zA-Z0-9]+:\/\/(?:[-a-zA-Z0-9]+\.)+[a-zA-Z]+\b[a-zA-Z0-9:%-_\+.~#?&\/=]*/g

function autolink(text) {
  return split(urlRe, text, (url) => {
    return {
      text: url,
      html: function() { return `<a href="${url}">${this.text}</a>` }
    }
  })
}

function secret(text) {
  return split(/\bsecret:\s*[^\s\r\n]+/g, text, (s) => {
    // NOTE: s.split can be avoided if regexp-utils.split calls the callback
    // with match object, instead of a string.
    const [blank, secret] = s.split(/^secret:\s*/)
    return {
      html: function() {
        return `secret: <span class="c-text--secret">${secret}</span>`
      }
    }
  })
}

function highlight(stems, text) {
  const array = split(/\w{2,}/g, text, (s) => {
    let stem = stemmer(s.toLowerCase())
    if (stems.includes(stem))
      return {
        text: s,
        html: function() {
          return `<span class="c-text--highlight">${this.text}</span>`
        }
      }
    return s
  })
  return array
  // return join(array)
}

function list(text) {
  return split(/^[\n\r]{0,1}(?:#\s+.+[\n\r]{0,1})+$[\n\r]{0,1}/mg, text, (s) => {
    const lines = s.trim().split(/[\n\r]+/),
          ol = {
            html: function() { return `<ol>${this.text}</ol>` }
          }
    ol.text = lines.map((line) => {
      const [blank, text] = line.split(/#\s+/)
      return {
        text: text,
        html: function() { return `<li>${this.text}</li>` }
      }
    })
    return ol
  })
}

const makeAry = [
  list,
  autolink
]

// Returns transformers list.
function make(options = {}) {
  let ary = makeAry
  // Add optional highlight transformer
  if (options.stems && options.stems.length)
    ary = ary.concat([
      function(text) {
        return highlight(options.stems, text)
      }
    ])
  return ary
}

module.exports = {make, autolink, secret, highlight, list}
