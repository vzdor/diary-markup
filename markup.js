'use strict';

const {split} = require('./regexp-utils')

const symbols = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;'
}

function escape(text) {
  return text.replace(/[<>&]/g, (symbol) => symbols[symbol])
}

function newLine(text) {
  return split(/[\r\n]/g, text, (nl) => {
    return {
      html: function() { return '<br />' }
    }
  })
}

// Array utility.
function flatten(arr) {
  return [].concat(...arr)
}

function transform(array, fu) {
  const tmp = array.map((el) => {
    if (typeof el === 'string') return fu(el)
    if (el.text instanceof Array)
      el.text = transform(el.text, fu)
    else if (el.text)
      el.text = fu(el.text)
    return el
  })
  return flatten(tmp)
}

function reduce(array) {
  return array.reduce((ac, el) => {
    if (el.text && el.text instanceof Array) el.text = reduce(el.text)
    return ac + (el.html ? el.html() : el)
  }, '')
}

// text: unescaped text.
// returns html.
function markup(text, {transformers = []} = {}) {
  let array = [escape(text)]
  transformers.forEach((fu) => array = transform(array, fu))
  // Call at the end -- some transformers may expect \n\r.
  array = transform(array, newLine)
  return reduce(array)
}

module.exports = markup
