'use strict';

// re: regexp
// s: string
// callback: a function
function split(re, s, callback) {
  const results = []

  let match,
      position = 0

  function substring(end) {
    return s.substring(position, end)
  }

  while (match = re.exec(s)) {
    results.push(substring(match.index))
    results.push(callback(match[0]))
    position = match.index + match[0].length
  }

  if (position < s.length) results.push(substring(s.length))

  return results
}

module.exports = {split}
