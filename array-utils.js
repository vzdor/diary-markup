'use strict';

// Joins consequent text nodes.
// NOTE: Not used.
function join(array) {
  return array.reduce((ac, el) => {
    if (ac.length === 0)
      ac.push(el)
    else {
      const i = ac.length - 1
      if (typeof el === 'string' && typeof ac[i] === 'string')
        ac[i] += el
      else
        ac.push(el)
    }
    return ac
  }, [])
}
