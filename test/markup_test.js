'use strict';

const markup = require('../markup')

const assert = require('assert')

describe('markup', function() {
  it('transforms new lines', function() {
    const text = markup("Hello,\n\nBob. Hello,\nJhon.")
    assert.equal(text, 'Hello,<br /><br />Bob. Hello,<br />Jhon.')
  })

  it('escapes html tags and amp', function() {
    const text = markup('<hello>&</hello>')
    assert.equal(text, '&lt;hello&gt;&amp;&lt;/hello&gt;')
  })
})
