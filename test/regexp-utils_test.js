'use strict';

const split = require('../regexp-utils').split

const assert = require('assert')

describe('regexp-utils', function() {
  describe('split', function() {
    it('collects objects', function() {
      const results = split(/\d+/g, 'x: 123$.', (n) => ({number: n}))
      assert.deepEqual(results, ['x: ', {number: '123'}, '$.'])
    })
  })
})
