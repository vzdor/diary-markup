'use strict';

const markup = require('../markup'),
      transformers = require('../transformers')

const assert = require('assert')

describe('transformers', function() {
  it('transforms secrets', function() {
    const text = markup("secret: @bob1#\n", {
      transformers: [transformers.secret]
    })
    assert.equal(text, "secret: <span class=\"c-text--secret\">@bob1#</span><br />")
  })

  describe('highlight transformer', function() {
    function highlight(text) {
      return transformers.highlight(['fish'], text)
    }

    it('highlights stem words', function() {
      const text = markup('fishing at the lake.', {
        transformers: [highlight]
      })
      assert.equal(text, '<span class="c-text--highlight">fishing</span> at the lake.')
    })

    it('highlights stem words inside a link', function() {
      const text = markup('http://fishing.xxx/pike', {
        transformers: [transformers.autolink, highlight]
      })
      assert.equal(text, '<a href="http://fishing.xxx/pike">http://<span class="c-text--highlight">fishing</span>.xxx/pike</a>')
    })
  })

  describe('list transformer', function() {
    it('returns list', function() {
      const text = markup("hello\n# new\n# tower", {
        transformers: [transformers.list]
      })
      assert.deepEqual(text, 'hello<br /><ol><li>new</li><li>tower</li></ol>')
    })

    it('trims last new line', function() {
      const text = markup("# new\n# tower\nbob", {
        transformers: [transformers.list]
      })
      assert.deepEqual(text, '<ol><li>new</li><li>tower</li></ol>bob')
    })

    it('trimps new line at the beginning', function() {
      const text = markup("\n# new\n# tower", {
        transformers: [transformers.list]
      })
      assert.deepEqual(text, '<ol><li>new</li><li>tower</li></ol>')
    })
  })

  describe('autolink transformer', function() {
    function autolink(text) {
      return  markup(text, {
        transformers: [transformers.autolink]
      })
    }

    it('transforms urls', function() {
      const text = autolink('An url http://hello.world/blah')
      assert.equal(text, 'An url <a href="http://hello.world/blah">http://hello.world/blah</a>')
    })

    it('transforms urls with amp', function() {
      const text = autolink('http://blah.blah/?x=1&y=2')
      assert.equal(text, '<a href="http://blah.blah/?x=1&amp;y=2">http://blah.blah/?x=1&amp;y=2</a>')
    })

    it('ignores urls without scheme', function() {
      const text = autolink('blah.blah')
      assert.equal(text, 'blah.blah')
    })
  })

  describe('make', function() {
    it('returns list', function() {
      assert.deepEqual(transformers.make(), [
        transformers.list,
        transformers.autolink
      ])
    })

    it('includes highlight', function() {
      let ary = transformers.make({stems: ['fish']})
      assert.equal(ary[0], transformers.list)
      assert.equal(ary[1], transformers.autolink)
      assert(ary[2])
      const [_, node] = ary[2]('fishing')
      assert(/highlight/.test(node.html()))
    })
  })
})
